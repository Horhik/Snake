# Simple snake game

  Play on codepen : https://codepen.io/horhik/pen/MWgJXba

## Start
just open **./app/index.html**

or

*install dependencies :*
```bash
npm install
```


*if you on linux :* 
```bash
sudo gulp
```

*if you on other systems :*
```bash
gulp
```

